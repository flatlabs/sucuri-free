/*
Numerical Integration - Pure C Version
Leandro Marzulo <leandro.marzulo@flatlabs.com.br>

Usage:
./NumericalIntegration <number_of_points> <computation_weight>
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double f(int numInt, int peso)
{
	double total = 0;
	int itTotal = (((numInt % 10) * peso) +1);
	int y;
	for(y=1; y<itTotal; y++)
		total += sqrt(y);
	return total;
	
}

int main(int argc, char ** argv)
{
	double s = 0;
	int n = atoi(argv[1]);
	int peso = atoi(argv[2]);
	int x;
	for (x=1; x<=n; x++)
		s += f(x, peso);
	printf("Result: %lf\n", s);
	return 0;
}
