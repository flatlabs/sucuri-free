import numpy.ctypeslib as npct

from ctypes import c_int
from ctypes import c_double

# load the library, using numpy mechanisms
libf = npct.load_library("libf", ".")

# setup the return types and argument types
libf.f.restype = c_double
libf.f.argtypes = [c_int, c_int]


def f_func(a,b):
    return libf.f(a, b)
