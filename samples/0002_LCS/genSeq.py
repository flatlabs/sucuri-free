import random
import sys

def gen(n):
	dna = ["a","g","c","t"]
	sequence=''
	for i in range(0,n):
		sequence+=random.choice(dna)
	return sequence
	
n = int(sys.argv[1])
f=open('testA'+str(n)+'.txt', 'w')
f.write(gen(n))
f.close()

f=open('testB'+str(n)+'.txt', 'w')
f.write(gen(n))
f.close()
