'''
Numerical Integration - Python Version with C library
Leandro Marzulo <leandro.marzulo@flatlabs.com.br>

Usage:
python NumericalIntegration.py <number_of_points> <computation_weight>
'''


import sys
import f



s = 0
n = int(sys.argv[1])
peso = int(sys.argv[2])

for x in xrange(1, n+1):
	s += f.f_func(x, peso)
print 'Result: %f' %s
