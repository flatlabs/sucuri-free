/*
Numerical Integration - C Version with MPI + OpenMP
Leandro Marzulo <leandro.marzulo@flatlabs.com.br>

Usage:
mpirun -n <mpi_processes> ./NumericalIntegration.mpiomp <number_of_points> <computation_weight> <openmp_threads>
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

double f(int numInt, int peso)
{
	double total = 0;
	int itTotal = (((numInt % 10) * peso) +1);
	int y;
	for(y=1; y<itTotal; y++)
		total += sqrt(y);
	return total;	
}

int main(int argc, char ** argv)
{
	int nprocs;
	int rank;
	MPI_Init(&argc, &argv);
    	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	double s = 0;
	int n = atoi(argv[1]);
	int peso = atoi(argv[2]);
	int nthreads = atoi(argv[3]);
	int x;
	//Calculate which iterations will be calculated by each process
	int block = n/nprocs;
	int start = rank * block + 1;
	int end = ((rank+1) == nprocs) ? n+1 : start+block;
	#pragma omp parallel for reduction(+:s) num_threads(nthreads)
	for (x=start; x<end; x++)
		s += f(x, peso);
	
	if (rank > 0) 
	{
		//Send partial results to master
		MPI_Send(&s, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
	else
	{
		//receive partial results and reduce
		double tmp;
		MPI_Status status;
		for (x=1; x<nprocs; x++)
		{
			MPI_Recv(&tmp, 1, MPI_DOUBLE, x, 0, MPI_COMM_WORLD, &status);
			s+=tmp;
		}
		printf("Result: %lf\n", s);
	}
	MPI_Finalize();
	return 0;
}
