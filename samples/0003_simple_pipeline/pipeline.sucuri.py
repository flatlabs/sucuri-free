'''
Simple Pipeline - Sucuri Version
Leandro Marzulo <leandro.marzulo@flatlabs.com.br>

Usage:
python pipeline.sucuri.py <numer_of_workers>
'''
import sys, os

#need this environment variable if Sucuri is installed manually.
sys.path.append(os.environ['SUCURIHOME'])
from sucuri import *


def print_line(args):
	line = args[0]
	print "-- " + line[:-1] + " --"

nprocs = int(sys.argv[1])

graph = DFGraph()
sched = Scheduler(graph, nprocs, mpi_enabled = False)
fp = open("text.txt", "r")

src = Source(fp)
printer = Serializer(print_line, 1)

printer.pin(0)
graph.add(src)
graph.add(printer)

src.add_edge(printer, 0)

sched.start()
